The goal is to enable easier workflows for deploying to remote targets
when cross compiling. Ideally the developer would do something like

```shell
$ export target=192.168.1.123
$ cmake /path/to/project
...
$ make deploy
```

Where the last step installs the project and deploys it to the
target. By default the deployment is done using rsync, but the
deployment script can be customized by creating a project specific
template in `${CMAKE_CURRENT_LIST_DIR}/templates/deploy`.
