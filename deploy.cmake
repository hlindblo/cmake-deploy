function(add_deploy host target_service)
  set(deploy "deploy")
  set(deploy_file "${CMAKE_CURRENT_BINARY_DIR}/${deploy}")
  set(deploy_install "deploy_install")
  set(template_dir "${CMAKE_CURRENT_LIST_DIR}/templates")
  set(template_file "${template_dir}/${deploy}")

  if (${template_file} IS_NEWER_THAN ${deploy_file})
    message(STATUS "Generating deploy script for ${host}")
    configure_file(${template_file} ${deploy})
    execute_process(COMMAND ${CMAKE_COMMAND} -E touch ${deploy_file})
    message(STATUS "Deployment script generated to ${deploy_file}")
  endif()

  set(target_name ${deploy})
  if (${ARGC} GREATER 2)
    set(target_name "${target_name}-${ARGV2}")
  endif()

  if (NOT TARGET ${target_name})
    message(STATUS "Adding deployment rule")
    add_custom_target(${target_name} chmod u+x ${deploy}
      COMMAND ${CMAKE_COMMAND} --build .
      COMMAND ${CMAKE_COMMAND} -DCMAKE_INSTALL_PREFIX=${deploy_install} -P cmake_install.cmake
      COMMAND ./${deploy}
      COMMENT "Deploying to target ${host}")

    get_filename_component(make ${CMAKE_MAKE_PROGRAM} NAME)
    message(STATUS "Deployment target added. Usage: '${make} ${target_name}'")
  endif()
endfunction()
